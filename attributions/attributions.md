This project could not exist without the work of several other people:

- Core Application functionality
  - Bevy [MIT](licenses/LICENSE-bevy)
    - rendering, state, asset management, etc.
  - RON [MIT](licenses/LICENSE-RON)
    - Application Configuration
  - StructOpt [MIT](licenses/LICENSE-StructOpt)
    - Command Line Interface
- Visuals
  - Fira [OFL](licenses/LICENSE-Fira)
    - Application font